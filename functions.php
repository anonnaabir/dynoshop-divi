        <?php
        if ( !defined( 'ABSPATH' ) ) { exit; }

        if ( ! defined( 'DYNOSHOP_VERSION' ) ) {
            define( 'DYNOSHOP_VERSION', '1.0' );
        }

        // Required Functions Start

        require get_stylesheet_directory() . '/inc/theme-setup.php';  // All Theme Setup Functions
        require get_stylesheet_directory() . '/inc/admin.php';  // Admin Panel by CSF
        require get_stylesheet_directory() . '/inc/demo-import.php';


        // Auto Updater Function

        // require 'lib/updater/plugin-update-checker.php';
        // $WPDynoshopUpdater = Puc_v4_Factory::buildUpdateChecker(
        // 'https://api.themespell.com/updater/?action=get_metadata&slug=wp-dynoshop',
        // __FILE__,
        // 'wp-dynoshop'
        // );
