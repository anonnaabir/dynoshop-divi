<?php

			// Demo Import Function Start

			function wp_dynoshop_demo_import() {
				return array(
					array(
						'import_file_name'           => 'Camera Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/camerashop.WordPress.2021-10-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_camerashop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/camera-shop/',
					),					

				);
			}
			add_filter( 'pt-ocdi/import_files', 'wp_dynoshop_demo_import' );



			function wp_dynoshop_demo_import_page_setup( $default_settings ) {
				$lp_options = get_option( 'dynoshop_options' );

				if (isset($lp_options['white_label_theme_name'])){
					$theme_name = $lp_options['white_label_theme_name'];
				}
				else {
					$theme_name = 'Dynoshop';
				}

				$default_settings['parent_slug'] = 'themes.php';
				$default_settings['page_title']  = esc_html__( $theme_name.' Demo Import' , 'wp-dynoshop' );
				$default_settings['menu_title']  = esc_html__( 'Import '.$theme_name.' Demos' , 'wp-dynoshop' );
				$default_settings['capability']  = 'import';
				// $default_settings['menu_slug']   = 'pt-one-click-demo-import';

				return $default_settings;
			}
			add_filter( 'pt-ocdi/plugin_page_setup', 'wp_dynoshop_demo_import_page_setup' );


			function wp_dynoshop_demo_import_page_title ($plugin_title ) {
				?>
				<h1 class="ocdi__title  dashicons-before  dashicons-upload"><?php $plugin_title  = esc_html_e( 'Dynoshop Demo Import', 'wp-dynoshop' ); ?></h1>
				<?php
			}
			add_filter( 'pt-ocdi/plugin_page_title', 'wp_dynoshop_demo_import_page_title' );

			add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

			add_filter( 'pt-ocdi/import_memory_limit', '256M' );





			// Demo Import Function End
